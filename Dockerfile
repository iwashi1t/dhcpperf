FROM --platform=linux/x86_64 centos:7

WORKDIR /dhcpperf

COPY docker-entrypoint.sh /dhcpperf/.
RUN chmod +x /dhcpperf/docker-entrypoint.sh

RUN yum -y install wget iproute
RUN wget https://www.ncad.co.jp/~prodhcp/dhcpperf/dhcpperf-0.3.6.el7.x86_64.rpm
RUN yum -y install dhcpperf-0.3.6.el7.x86_64.rpm

ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["eth0"]
