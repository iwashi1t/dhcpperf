# dhcpperf

Environment to run dhcpperf using multipass and docker.

https://www.ncad.co.jp/~prodhcp/download.html

## Environment

- macOS 12.4
- multipass 1.10.1 (on macOS)
- Ubuntu 22.04 (on multipass)
- CentOS 7 (on docker)

## Install multipass

```sh
brew install --cask multipass
```

## Setup with multipass

```sh
multipass set local.bridged-network={DEVICE}
multipass launch --bridged -n docker docker
multipass mount {DHCPPERF_PATH} docker:/home/ubuntu/dhcpperf
```

- To find `{DEVICE}`, connect to the target network by settings, then use `ifconfig` on host (macOS).
- `{DHCPPERF_PATH}` is the path to `dhcpperf` directory.
- When the network is changed, `multipass restart docker` must be done.

## Do test

### Enter multipass

```sh
multipass shell docker
```

### Build docker image

```sh
cd dhcpperf
docker build -t dhcpperf .
```

### Run dhcpperf

```sh
cd dhcpperf
docker run -it --privileged \
  --platform linux/x86_64 \
  --network host \
  --volume $(pwd)/output:/dhcpperf/output \
  dhcpperf \
  {DEVICE}
```

- To find `{DEVICE}`, use `ip addr` on host (multipass).

then dhcpperf test performed => files `{TIMESTAMP}.log` and `{TIMESTAMP}.debug.log` will be generated in directory `output`.

## Trouble shooting

### multipass: ssh connection failed: 'Network is unreachable'

```sh
multipass delete docker
multipass recover docker
multipass start docker
multipass shell docker
```
