#! /bin/bash

DATE=$(date +%s)
dhcpperf -v --log-file output/$DATE.debug.log --timeout 10 $@ | tee output/$DATE.log
